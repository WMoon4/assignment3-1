//
//  ViewController.swift
//  Assignment3-1
//
//  Created by V.K. on 2/8/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
    
        countCharacters()
        
        checkSuffix()
        
        splitNames()
        
        reverseString()
        
        separateThousands()
        
        checkPasswordValidityTwo()
        
    }
    
    func countCharacters() {
        let str = "Jack"
        print("---------- 1 ----------")
        print("String is [\(str)]")
        
        print("Counted once: \(str.count) characters")
        var counter = 0
        for _ in str {
            counter += 1
        }
        print("Counted twice: \(counter) characters")
    }

    func checkSuffix() {
        //let str = "Actor"
        let str = "Actress"
        print("---------- 2 ----------")
        print("String is [\(str)]")
        
        if str.hasSuffix("or") {
            print("has suffix '-or'")
        } else if str.hasSuffix("ress") {
            print("has suffix '-ress'")
        } else {
            print("neither '-or' nor '-ress' suffix")
        }
        
    }
    
    func splitNames() {
        let str = "JackBountyHunter"
        print("---------- 3 ----------")
        print("String is [\(str)]")
        
        var splitBySpace = ""
        var currName = ""
        for character in str {
            if character >= "A" && character <= "Z" {
                // print prev name
                if !currName.isEmpty {
                    print(currName)
                    splitBySpace += currName + " "
                }
                // start collecting new name
                currName = String(character)
            } else {
                currName.append(character)
            }
        }
        print(currName)  // last one
        splitBySpace += currName
        print(splitBySpace)
    }
    
    func reverseString() {
        let str = "Innuendo"
        print("---------- 4 ----------")
        print("String is [\(str)]")
        
        var revStr = ""
        for index in str.indices.reversed() {
            revStr.append(str[index])
        }
        print("reversed is \(revStr)")
    }
    
    func separateThousands() {
        var str = "1234567890"
        print("---------- 5 ----------")
        print("String is [\(str)]")
        assert(Int(str) != nil, "String does not contain integer")
        
        var count: Int = 0
        for index in str.indices.reversed() {
            count += 1
            if count == 3 {
                str.insert(",", at: index)
                count = 0
            }
        }
        print("separated is \(str)")
    }
    
      
    class PswdFeature {
        var featApplied: Bool
        let reportStr: String
        let checkFeature: (Character)->Bool  // this checker returns true if character has the feature
        
        init(str: String, checker: @escaping (Character)->Bool) {
            self.featApplied = false
            self.reportStr = str
            self.checkFeature = checker
        }
        
        func checkCharacter(ch: Character)->Void {
            if self.checkFeature(ch) {
                self.featApplied = true
            }
        }
    }
    
    let features: [PswdFeature] = [PswdFeature(str: " a)", checker: { $0.isNumber }),
                                   PswdFeature(str: " b)", checker: { $0.isUppercase }),
                                   PswdFeature(str: " c)", checker: { $0.isLowercase }),
                                   PswdFeature(str: " d)", checker: { $0.isMathSymbol || $0.isPunctuation })]
    
    
    func checkPasswordValidityTwo() {
        let str = "ZZ1!2*%3456a78,<90"
        print("---------- 6 ----------")
        print("String is [\(str)]")
        // scan string
        for character in str {
            for feature in features {
                feature.checkCharacter(ch: character)
            }
        }
        // count features
        var featureCount: Int = 0
        var featureList = ""
        for feature in features {
            if feature.featApplied {
                featureCount += 1
                featureList += feature.reportStr
            }
        }
        if featureCount == 4 {
            featureList = " e)"
        }
        print("password validity check: \(featureCount)\(featureList)")
        
    }

/*
    func checkPasswordValidity() {
        let str = "12Z3456a7890"
        print("---------- 6 ----------")
        print("String is [\(str)]")
        
        var featureCount: Int = 0
        var featureList = ""
        var hasDigits = false
        var hasCapitals = false
        var hasRegulars = false
        for character in str {
            switch character {
            case _ where character >= "0" && character <= "9":
                if !hasDigits {
                    hasDigits = true
                    featureCount += 1
                    featureList += " a)"
                }
            case _ where character >= "A" && character <= "Z":
                if !hasCapitals {
                    hasCapitals = true
                    featureCount += 1
                    featureList += " b)"
                }
            case _ where character >= "A" && character <= "Z":
                if !hasRegulars {
                    hasRegulars = true
                    featureCount += 1
                    featureList += " c)"
                }
            default:
                print("not valid symbol found!")
                return
            }
        }
        print("password validity check: \(featureCount)\(featureList)")
    }
*/

}

